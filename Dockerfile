FROM php:7.4.32-fpm

RUN apt update && apt upgrade

# install composer
RUN curl -sSL https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www/halal

EXPOSE 8000

# Running service
CMD ["php", "artisan", "serve"]
