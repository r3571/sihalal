@extends('layouts.main')

@section('content')
<div class="page-heading">
    <h3>Profile #UserName</h3>
</div>
<div class="page-content">
    <section class="row">
        <div class="col-12">
            {{-- <div class="row">
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon purple">
                                        <i class="iconly-boldShow"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Profile Views</h6>
                                    <h6 class="font-extrabold mb-0">112.000</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon blue">
                                        <i class="iconly-boldProfile"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Followers</h6>
                                    <h6 class="font-extrabold mb-0">183.000</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon green">
                                        <i class="iconly-boldAdd-User"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Following</h6>
                                    <h6 class="font-extrabold mb-0">80.000</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon red">
                                        <i class="iconly-boldBookmark"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Saved Post</h6>
                                    <h6 class="font-extrabold mb-0">112</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Data Profile</h4>
                        </div>
                        <div class="card-body">
                            {{-- <div id="chart-profile-visit"></div> --}}
                            <div class="row">
                                <div class="col-2">
                                    <h6>Name<h6>
                                </div>
                                <div class="col-1">
                                    <h6>:<h6>
                                </div>
                                <div class="col-9">
                                    <h6>INDRA SUWITO<h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    <h6>Addres<h6>
                                </div>
                                <div class="col-1">
                                    <h6>:<h6>
                                </div>
                                <div class="col-9">
                                    <h6>PJL. SIMPANG SERUWAI LINK XVIII SEI MATI MEDAN
                                        LABUHAN<h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    <h6>Type of Business<h6>
                                </div>
                                <div class="col-1">
                                    <h6>:<h6>
                                </div>
                                <div class="col-9">
                                    <h6>JBU.8<h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    <h6>Email<h6>
                                </div>
                                <div class="col-1">
                                    <h6>:<h6>
                                </div>
                                <div class="col-9">
                                    <h6>indrasuwito605@gmail.com<h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    <h6>Telephone Number<h6>
                                </div>
                                <div class="col-1">
                                    <h6>:<h6>
                                </div>
                                <div class="col-9">
                                    <h6>081133445567<h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                {{-- <div class="col-12 col-xl-4">
                    <div class="card">
                        <div class="card-header">
                            <h4>Profile Visit</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="d-flex align-items-center">
                                        <svg class="bi text-primary" width="32" height="32" fill="blue" style="width:10px">
                                            <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#circle-fill" />
                                        </svg>
                                        <h5 class="mb-0 ms-3">Europe</h5>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <h5 class="mb-0">862</h5>
                                </div>
                                <div class="col-12">
                                    <div id="chart-europe"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="d-flex align-items-center">
                                        <svg class="bi text-success" width="32" height="32" fill="blue" style="width:10px">
                                            <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#circle-fill" />
                                        </svg>
                                        <h5 class="mb-0 ms-3">America</h5>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <h5 class="mb-0">375</h5>
                                </div>
                                <div class="col-12">
                                    <div id="chart-america"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="d-flex align-items-center">
                                        <svg class="bi text-danger" width="32" height="32" fill="blue" style="width:10px">
                                            <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#circle-fill" />
                                        </svg>
                                        <h5 class="mb-0 ms-3">Indonesia</h5>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <h5 class="mb-0">1025</h5>
                                </div>
                                <div class="col-12">
                                    <div id="chart-indonesia"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            {{-- <h4>Latest Comments</h4> --}}
                            {{-- <button type="button" class="btn btn-primary">
                                <svg class="svg-inline--fa fa-plus-square fa-w-14 fa-fw select-all" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                    <path fill="currentColor" d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-32 252c0 6.6-5.4 12-12 12h-92v92c0 6.6-5.4 12-12 12h-56c-6.6 0-12-5.4-12-12v-92H92c-6.6 0-12-5.4-12-12v-56c0-6.6 5.4-12 12-12h92v-92c0-6.6 5.4-12 12-12h56c6.6 0 12 5.4 12 12v92h92c6.6 0 12 5.4 12 12v56z"></path>
                                </svg> Add
                            </button> --}}
                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title">Detail Profile</h5>
                                </div>
                                <div class="card-body">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link active" id="perusahaan-tab" data-bs-toggle="tab" href="#perusahaan" role="tab" aria-controls="perusahaan" aria-selected="true">Factory</a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="produk-tab" data-bs-toggle="tab" href="#produk" role="tab" aria-controls="produk" aria-selected="false">Product</a>
                                        </li>
                                        {{-- <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
                                        </li> --}}
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="perusahaan" role="tabpanel" aria-labelledby="home-tab">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-lg">
                                                    <thead>
                                                        <tr>
                                                            <th>Factory Name</th>
                                                            <th>Addres</th>
                                                            <th>City</th>
                                                            <th>Province</th>
                                                            <th>Country</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="col-auto">
                                                                <p class="mb-0">Indra Jaya Bakery</p>
                                                            </td>
                                                            <td class="col-auto">
                                                                <p class=" mb-0">Jl. Simpang Seruwai Link XVIII, Kel. Sei
                                                                    Mati, Kec. Medan Labuhan, Kota Medan, Provinsi Sumatera Utara 20252</p>
                                                            </td>
                                                            <td class="col-auto">
                                                                <p class=" mb-0">Medan</p>
                                                            </td>
                                                            <td class="col-auto">
                                                                <p class=" mb-0">Sumatra Utara</p>
                                                            </td>
                                                            <td class="col-auto">
                                                                <p class=" mb-0">Indonesia</p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="produk" role="tabpanel" aria-labelledby="produk-tab">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-lg">
                                                    <thead>
                                                        <tr>
                                                            <th>Product Id</th>
                                                            <th>Product Name</th>
                                                            <th>Factory</th>
                                                            <th>Product Picture</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="col-auto">
                                                                <p class="mb-0">928957</p>
                                                            </td>
                                                            <td class="col-auto">
                                                                <p class=" mb-0">Donat Tepung</p>
                                                            </td>
                                                            <td class="col-auto">
                                                                <p class=" mb-0">Indra Jaya Bakery</p>
                                                            </td>
                                                            <td class="col-auto">
                                                                <p class=" mb-0"><i class="bi bi-image"></i></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-auto">
                                                                <p class="mb-0">928958</p>
                                                            </td>
                                                            <td class="col-auto">
                                                                <p class=" mb-0">Donat Lumer</p>
                                                            </td>
                                                            <td class="col-auto">
                                                                <p class=" mb-0">Indra Jaya Bakery</p>
                                                            </td>
                                                            <td class="col-auto">
                                                                <p class=" mb-0"><i class="bi bi-image"></i></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-auto">
                                                                <p class="mb-0">928959</p>
                                                            </td>
                                                            <td class="col-auto">
                                                                <p class=" mb-0">Donat mini</p>
                                                            </td>
                                                            <td class="col-auto">
                                                                <p class=" mb-0">Indra Jaya Bakery</p>
                                                            </td>
                                                            <td class="col-auto">
                                                                <p class=" mb-0"><i class="bi bi-image"></i></p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                            <p class="mt-2">Duis ultrices purus non eros fermentum hendrerit. Aenean ornare interdum
                                                viverra. Sed ut odio velit. Aenean eu diam
                                                dictum nibh rhoncus mattis quis ac risus. Vivamus eu congue ipsum. Maecenas id
                                                sollicitudin ex. Cras in ex vestibulum,
                                                posuere orci at, sollicitudin purus. Morbi mollis elementum enim, in cursus sem
                                                placerat ut.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="col-12 col-lg-3">
            <div class="card">
                <div class="card-body py-4 px-5">
                    <div class="d-flex align-items-center">
                        <div class="avatar avatar-xl">
                            <img src="assets/images/faces/1.jpg" alt="Face 1">
                        </div>
                        <div class="ms-3 name">
                            <h5 class="font-bold">John Duck</h5>
                            <h6 class="text-muted mb-0">@johnducky</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4>Recent Messages</h4>
                </div>
                <div class="card-content pb-4">
                    <div class="recent-message d-flex px-4 py-3">
                        <div class="avatar avatar-lg">
                            <img src="assets/images/faces/4.jpg">
                        </div>
                        <div class="name ms-4">
                            <h5 class="mb-1">Hank Schrader</h5>
                            <h6 class="text-muted mb-0">@johnducky</h6>
                        </div>
                    </div>
                    <div class="recent-message d-flex px-4 py-3">
                        <div class="avatar avatar-lg">
                            <img src="assets/images/faces/5.jpg">
                        </div>
                        <div class="name ms-4">
                            <h5 class="mb-1">Dean Winchester</h5>
                            <h6 class="text-muted mb-0">@imdean</h6>
                        </div>
                    </div>
                    <div class="recent-message d-flex px-4 py-3">
                        <div class="avatar avatar-lg">
                            <img src="assets/images/faces/1.jpg">
                        </div>
                        <div class="name ms-4">
                            <h5 class="mb-1">John Dodol</h5>
                            <h6 class="text-muted mb-0">@dodoljohn</h6>
                        </div>
                    </div>
                    <div class="px-4">
                        <button class='btn btn-block btn-xl btn-light-primary font-bold mt-3'>Start
                            Conversation</button>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4>Visitors Profile</h4>
                </div>
                <div class="card-body">
                    <div id="chart-visitors-profile"></div>
                </div>
            </div>
        </div> --}}
    </section>
</div>
@endsection

