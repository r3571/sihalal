<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Apps - Halal</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/vendors/bootstrap-icons/bootstrap-icons.css">
    <link rel="stylesheet" href="assets/css/app.css">
    <link rel="stylesheet" href="assets/css/pages/auth.css">
</head>

<body>
    <div id="auth">

        <div class="row h-100">
            <div class="col-lg-5 col-12">
                <div id="auth-left">
                    <div class="auth-logo"></div>
                    <h1 class="auth-title">Log in</h1>
                    <p class="auth-subtitle mb-5">Login with registered access at BPJPH.</p>

                    <form action="{{ route('authenticate') }}" method="POST">
                       @csrf
                        <div class="form-group position-relative has-icon-left mb-4">
                            <input 
                                type="text" 
                                name="email" 
                                class="form-control form-control-xl" 
                                placeholder="Email" 
                                value="lphwalisongo@walisongo.ac.id"
                                required 
                                autofocus
                            >
                            <div class="form-control-icon">
                                <i class="bi bi-person"></i>
                            </div>
                        </div>
                        <div class="form-group position-relative has-icon-left mb-4">
                            <input 
                                type="password" 
                                class="form-control 
                                form-control-xl" 
                                name="password" 
                                placeholder="Password" 
                                value="12345678"
                                required
                            >
                            <div class="form-control-icon">
                                <i class="bi bi-shield-lock"></i>
                            </div>
                        </div>
                        <button class="btn btn-block btn-lg shadow-lg mt-5 text-light" style="background-color:#0F5132">Log in</button>
                    </form>
                    <div class="text-center mt-5 text-lg fs-4">
                        <p><a class="font-bold" href="{{ route('forget') }}">Forgot password?</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 d-none d-lg-block">
                <div id="auth-right" class="text-center align-middle">
                    <img src="assets/images/logo/mujahidin.png" alt="Logo" style="width:90%; margin-top: 265px">
                </div>
            </div>
        </div>

    </div>
</body>

</html>
