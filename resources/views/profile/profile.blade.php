@extends('layouts.main')

@section('content')
<div class="page-heading">
    <h3>Profile</h3>
</div>
<div class="page-content">
    <section class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Data Profile</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-2">
                                    <h6>ID<h6>
                                </div>
                                <div class="col-1">
                                    <h6>:<h6>
                                </div>
                                <div class="col-9">
                                    <h6>{{ Session::get('userid') }}<h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    <h6>Nama<h6>
                                </div>
                                <div class="col-1">
                                    <h6>:<h6>
                                </div>
                                <div class="col-9">
                                    <h6>{{ Session::get('name') }}<h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    <h6>role<h6>
                                </div>
                                <div class="col-1">
                                    <h6>:<h6>
                                </div>
                                <div class="col-9">
                                    <h6>{{ Session::get('role') }}<h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
