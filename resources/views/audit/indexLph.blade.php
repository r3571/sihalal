@extends('layouts.main')

@section('content')
<section class="section">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">List Audit</h4>
        </div>
        <!-- Basic Tables start -->
		    <section class="section">
		        <div class="card">
		            <div class="card-body">
		                <table class="table" id="table1">
		                    <thead>
		                        <tr>
		                            <th>ID Reg</th>
		                            <th>Nama Pemohon</th>
		                            <th>Alias</th>
		                            <th>Status Reg</th>
		                            <th>No Daftar</th>
		                            <th>Tgl Daftar</th>
		                            <th>Jenis Daftar</th>
		                            <th>Jenis Product</th>
		                            <th>Jumlah Produk</th>
		                            <th>Jenis Usaha</th>
		                            <th>No Urut NDPU</th>
		                            <th>Action</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                        <tr>
		                            <td>123123</td>
		                            <td>Budi</td>
		                            <td>Buday</td>
		                            <td>
		                                <span class="badge bg-warning">Progress</span>
		                            </td>
		                            <td>123aaa</td>
		                            <td>20 Sept 2022</td>
		                            <td>Daftar Halal</td>
		                            <td>Kue Kering</td>
		                            <td>10</td>
		                            <td>Rumahan</td>
		                            <td>9999999</td>
		                            <td>
		                                <a href="/auditLPH/detailLPH" class="btn btn-sm btn-info">
		                                	<svg class="bi" width="1em" height="1em" fill="currentColor">
				                            	<use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
				                            </svg>
				                        </a>
		                            </td>
		                        </tr>
		                        <!-- <tr>
		                            <td>Kue Basah</td>
		                            <td>1982379</td>
		                            <td>LPH A</td>
		                            <td>5</td>
		                            <td>
		                                <span class="badge bg-warning">Progress</span>
		                            </td>
		                            <td>
		                                <a href="#" class="btn btn-sm btn-info">
		                                	<svg class="bi" width="1em" height="1em" fill="currentColor">
				                            	<use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
				                            </svg>
		                                </a>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>Kue Becek</td>
		                            <td>3546978</td>
		                            <td>PLH C</td>
		                            <td>6</td>
		                            <td>
		                                <span class="badge bg-success">Scheduled</span>
		                            </td>
		                            <td>
		                                <a href="#" class="btn btn-sm btn-info">
		                                	<svg class="bi" width="1em" height="1em" fill="currentColor">
				                            	<use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
				                            </svg>
		                                </a>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>Jus Mangga</td>
		                            <td>2546798</td>
		                            <td>PLH E</td>
		                            <td>6</td>
		                            <td>
		                                <span class="badge bg-warning">Progress</span>
		                            </td>
		                            <td>
		                                <a href="#" class="btn btn-sm btn-info">
		                                	<svg class="bi" width="1em" height="1em" fill="currentColor">
				                            	<use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
				                            </svg>
		                                </a>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>Jus Apluket</td>
		                            <td>6557896</td>
		                            <td>LPH D</td>
		                            <td>7</td>
		                            <td>
		                                <span class="badge bg-warning">Progress</span>
		                            </td>
		                            <td>
		                                <a href="#" class="btn btn-sm btn-info">
		                                	<svg class="bi" width="1em" height="1em" fill="currentColor">
				                            	<use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
				                            </svg>
		                                </a>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>Jus Ama</td>
		                            <td>6546547</td>
		                            <td>LPH W</td>
		                            <td>9</td>
		                            <td>
		                                <span class="badge bg-warning">Progress</span>
		                            </td>
		                            <td>
		                                <a href="#" class="btn btn-sm btn-info">
		                                	<svg class="bi" width="1em" height="1em" fill="currentColor">
				                            	<use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
				                            </svg>
		                                </a>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>Jus Yu Ar</td>
		                            <td>8976523</td>
		                            <td>LPH I</td>
		                            <td>8</td>
		                            <td>
		                                <span class="badge bg-success">Scheduled</span>
		                            </td>
		                            <td>
		                                <a href="#" class="btn btn-sm btn-info">
		                                	<svg class="bi" width="1em" height="1em" fill="currentColor">
				                            	<use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
				                            </svg>
		                                </a>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>Tahu Bulat</td>
		                            <td>9546147</td>
		                            <td>LPH U</td>
		                            <td>12</td>
		                            <td>
		                                <span class="badge bg-success">Scheduled</span>
		                            </td>
		                            <td>
		                                <a href="#" class="btn btn-sm btn-info">
		                                	<svg class="bi" width="1em" height="1em" fill="currentColor">
				                            	<use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
				                            </svg>
		                                </a>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>Tahu Segitiga</td>
		                            <td>5464778</td>
		                            <td>LPH Y</td>
		                            <td>11</td>
		                            <td>
		                                <span class="badge bg-success">Scheduled</span>
		                            </td>
		                            <td>
		                                <a href="#" class="btn btn-sm btn-info">
		                                	<svg class="bi" width="1em" height="1em" fill="currentColor">
				                            	<use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
				                            </svg>
		                                </a>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>Tahu Kotak</td>
		                            <td>5464778</td>
		                            <td>LPH T</td>
		                            <td>11</td>
		                            <td>
		                                <span class="badge bg-success">Scheduled</span>
		                            </td>
		                            <td>
		                                <a href="#" class="btn btn-sm btn-info">
		                                	<svg class="bi" width="1em" height="1em" fill="currentColor">
				                            	<use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
				                            </svg>
		                                </a>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>Tahu Spiral</td>
		                            <td>5464778</td>
		                            <td>LPH P</td>
		                            <td>11</td>
		                            <td>
		                                <span class="badge bg-success">Scheduled</span>
		                            </td>
		                            <td>
		                                <a href="#" class="btn btn-sm btn-info">
		                                	<svg class="bi" width="1em" height="1em" fill="currentColor">
				                            	<use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
				                            </svg>
		                                </a>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>Ayam Bakar</td>
		                            <td>5464778</td>
		                            <td>LPH Y</td>
		                            <td>11</td>
		                            <td>
		                                <span class="badge bg-success">Scheduled</span>
		                            </td>
		                            <td>
		                                <a href="#" class="btn btn-sm btn-info">
		                                	<svg class="bi" width="1em" height="1em" fill="currentColor">
				                            	<use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
				                            </svg>
		                                </a>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>Ayam Goreng</td>
		                            <td>5464778</td>
		                            <td>LPH Y</td>
		                            <td>11</td>
		                            <td>
		                                <span class="badge bg-success">Scheduled</span>
		                            </td>
		                            <td>
		                                <a href="#" class="btn btn-sm btn-info">
		                                	<svg class="bi" width="1em" height="1em" fill="currentColor">
				                            	<use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
				                            </svg>
		                                </a>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>Rendang</td>
		                            <td>5464778</td>
		                            <td>LPH Y</td>
		                            <td>11</td>
		                            <td>
		                                <span class="badge bg-success">Scheduled</span>
		                            </td>
		                            <td>
		                                <a href="#" class="btn btn-sm btn-info">
		                                	<svg class="bi" width="1em" height="1em" fill="currentColor">
				                            	<use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
				                            </svg>
		                                </a>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>Telor Dadar</td>
		                            <td>5464778</td>
		                            <td>LPH Y</td>
		                            <td>11</td>
		                            <td>
		                                <span class="badge bg-success">Scheduled</span>
		                            </td>
		                            <td>
		                                <a href="#" class="btn btn-sm btn-info">
		                                	<svg class="bi" width="1em" height="1em" fill="currentColor">
				                            	<use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
				                            </svg>
		                                </a>
		                            </td>
		                        </tr> -->
		                    </tbody>
		                </table>
		            </div>
		        </div>
		    </section>
	    <!-- Basic Tables end -->
    </div>
</section>
@endsection