@extends('layouts.main')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
@section('content')
<div class="page-content">
    <section class="row">
        <div class="col-12">              
            <div class="card">
                <div class="card-body">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Detail Audit</h5>
                        </div>
                        @if(session()->has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{session('success')}}
                            <button type="button" class="btn-close" data-bs-dimiss="alert" arial-label="close"></button>
                        </div>
                        @endif
                        @if(session()->has('error'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('error')}}
                            <button type="button" class="btn-close" data-bs-dimiss="alert" arial-label="close"></button>
                        </div>
                        @endif
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" 
                                        id="biaya-tab" 
                                        data-bs-toggle="tab" 
                                        href="#biaya" 
                                        role="tab" 
                                        aria-controls="biaya" 
                                        aria-selected="true">Jadwal Audit
                                    </a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" 
                                        id="produk-tab" 
                                        data-bs-toggle="tab" 
                                        href="#produk" 
                                        role="tab" 
                                        aria-controls="produk" 
                                        aria-selected="false">Auditor
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div 
                                    class="tab-pane fade show active" 
                                    id="biaya" 
                                    role="tabpanel" 
                                    aria-labelledby="biaya-tab"
                                >
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <div class="mb-3">
                                            <button class="btn btn-outline-success block" onclick="history.back()"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left-square-fill" viewBox="0 0 16 16">
                                            <path d="M16 14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12zm-4.5-6.5H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5a.5.5 0 0 0 0-1z"/>
                                            </svg> Kembali</button>
                                                <button type="button" class="btn btn-outline-success block" data-bs-toggle="modal"
                                                    data-bs-target="#tambahJadwal">Tambah Jadwal <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-square-fill" viewBox="0 0 16 16">
                                                    <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3a.5.5 0 0 1 1 0z"/>
                                                    </svg>
                                                </button>
                                            </div>
                                        <table class="table" id="table1">
                                            <thead>
                                                <tr>
                                                    <th>Jadwal Awal</th>
                                                    <th>Jadwal Akhir</th>
                                                    <th>Total Hari</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($audit->payload AS $row)
                                                @if($row->id_reg == $idReg)
                                                <tr>
                                                    <td>{{$row->jadwal_awal}}</td>
                                                    <td>{{$row->jadwal_akhir}}</td>
                                                    <td>{{$row->jml_hari}}</td>
                                                    <td><a href="{{ route('deleteAudit', array( $row->id_audit)) }}" class="btn icon btn-danger">Hapus</a></td>
                                                </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                </div>
                                <div class="tab-pane fade" id="produk" role="tabpanel" aria-labelledby="produk-tab">
                                    <div class="table-responsive">
                                        <div class="mb-3 mt-3">
                                            <button class="btn btn-outline-success block" onclick="history.back()"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left-square-fill" viewBox="0 0 16 16">
                                            <path d="M16 14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12zm-4.5-6.5H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5a.5.5 0 0 0 0-1z"/>
                                            </svg> Kembali</button>
                                                <button type="button" class="btn btn-outline-success block" data-bs-toggle="modal"
                                                    data-bs-target="#tambahAuditor">Tambah Auditor <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-square-fill" viewBox="0 0 16 16">
                                                    <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3a.5.5 0 0 1 1 0z"/>
                                                    </svg>
                                                </button>
                                            </div>
                                        <table class="table" id="table2">
                                            <thead>
                                                <tr>
                                                    <th>Id Audit Person</th>
                                                    <th>Id Auditor</th>
                                                    <th>Nama</th>
                                                    <th>Bidang</th>
                                                    <th>Email</th>
                                                    <th>Handphone</th>
                                                    <th>Create By</th>
                                                    <th>Create On</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- aslkdjalsdjalskjdlaskjdlkasjd  -->
    <div class="modal fade" id="tambahJadwal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
            role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Jadwal Audit
                    </h5>
                    <button type="button" class="close" data-bs-dismiss="modal"
                        aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <form action="{{ route('create') }}" method="POST">
                @csrf
                <input type="hidden" class="form-control" name="id_reg" id="id_reg" value="{{$idReg}}">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="jadwal_awal">Jadwal Awal</label>
                        <input class="form-control tgl-awal"  name="jadwal_awal" id="jadwal_awal" placeholder="" onchange="hasil();">
                    </div>

                    <div class="form-group">
                        <label for="jadwal_akhir">Jadwal Akhir</label>
                        <input class="form-control tgl-akhir"  name="jadwal_akhir" id="jadwal_akhir" placeholder="" onchange="hasil();">
                    </div>

                    <div class="form-group">
                        <label for="jml_hari">Jumlah Hari</label>
                        <input type="text" id="jml_hari" name="jml_hari" class="form-control jlmh" placeholder="" onchange="hasil();" readonly>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-secondary"
                        data-bs-dismiss="modal">
                        <i class="bx bx-x d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Keluar</span>
                    </button>
                    <button type="submit" class="btn btn-primary ml-1">
                        <i class="bx bx-check d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Simpan</span>
                    </button>
                </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="tambahAuditor" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
            role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Auditor
                    </h5>
                    <button type="button" class="close" data-bs-dismiss="modal"
                        aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <form action="{{ route('AddAuditor') }}" method="POST">
                @csrf
                <input type="hidden" class="form-control" name="id_reg" id="id_reg" value="{{$idReg}}">
                <input type="hidden" class="form-control" name="lph" id="lph" value="{{ Session::get('name') }}">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="jadwal_awal1">Select Auditor</label>
                        <select class="form-select" aria-label="Default select example" id="auditor_id" name="auditor_id">
                            <option selected>Open this select menu</option>
                            @foreach($auditor->payload AS $row)
                                <option value="{{$row->auditor_id}}">{{$row->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-secondary"
                        data-bs-dismiss="modal">
                        <i class="bx bx-x d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Keluar</span>
                    </button>
                    <button type="submit" class="btn btn-primary ml-1">
                        <i class="bx bx-check d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Simpan</span>
                    </button>
                <!-- </form> -->
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function hasil(){
        var awal = document.getElementById("jadwal_awal").value;
        var akhir = document.getElementById("jadwal_akhir").value;
        var date1 = new Date(awal);
        var date2 = new Date(akhir);
        var oneDay = 24*60*60*1000;
        var total = Math.round(Math.abs((date2.getTime() - date1.getTime())/(oneDay)+1));
        if (!isNaN(total)){
            document.getElementById("jml_hari").value=total;
        }
    }
    $('.tgl-awal').datepicker({
    dateFormat: 'yy/mm/dd',
    changeMonth: true,
    changeYear: true,
});
$('.tgl-akhir').datepicker({
    dateFormat: 'yy/mm/dd',
    changeMonth: true,
    changeYear: true,
});
</script>
@endsection