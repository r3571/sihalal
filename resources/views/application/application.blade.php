@extends('layouts.main')

@section('content')
<section class="section">
	<div class="row" id="table-responsive">
	  <div class="col-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">List Permohonan</h4>
				@if (session()->has('Success'))
					<div class="alert alert-success alert-dismissible fade show" role="alert">
					{{ session('Success') }}
					<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
					</div>
				@endif
			</div>
			<div classs="row">
				<div class="card">
					<section class="section">
						<div class="card">
							{{-- <div class="card-header">
								<h4>List Produk</h4>
							</div> --}}
							<div class="card-body">
								<div class="table-responsive">
									<table class="table" id="table1">
										<thead>
											<tr>
												<th>Aksi</th>
												<th>ID Reg</th>
												<th>Nama Pemohon</th>
												<th>Status Reg</th>
												<th>No Daftar</th>
												<th>Tgl Daftar</th>
												<th>Jenis Daftar</th>
												<th>Jenis Product</th>
												<th>Jumlah Produk</th>
												<th>Jenis Usaha</th>
												<th>No Urut NDPU</th>
											  </tr>
										</thead>
										<tbody>
											@foreach($data->payload AS $row)
												<tr>
													<td>
														<a href="{{ route('detail', array($row->id_reg)) }}" class="btn btn-sm btn-info">
														<svg class="bi" width="1em" height="1em" fill="currentColor">
															<use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
														</svg>
														</a>
													</td>
													<td>{{$row->id_reg}}</td>
													<td>{{$row->nama_pu}}</td>
													<td>{{$row->nama_status_reg}}</td>
													<td>{{$row->no_daftar}}</td>
													<td>{{$row->tgl_daftar}}</td>
													<td>{{$row->nama_jenis_daftar}}</td>
													<td>{{$row->nama_jenis_produk}}</td>
													<td>{{$row->jml_produk}}</td>
													<td>{{$row->nama_jenis_usaha}}</td>
													<td>{{$row->no_ndpu}}</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </section>
@endsection