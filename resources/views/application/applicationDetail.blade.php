@extends('layouts.main')

@section('content')
    <div class="page-heading">
        <h3>Detail Data</h3>
        <div class="mb-3 mt-3">
            <button class="btn btn-outline-success block" onclick="history.back()">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left-square-fill" viewBox="0 0 16 16">
                    <path d="M16 14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12zm-4.5-6.5H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5a.5.5 0 0 0 0-1z"/>
                </svg> 
                Go Back
            </button>
        </div>
        @if (session()->has('Error'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('Error') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
    </div>
    <div class="page-content">
        <section class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Description</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-3">
                                        <h6>Nama<h6>
                                    </div>
                                    <div class="col-1">
                                        <h6>:<h6>
                                    </div>
                                    <div class="col-8">
                                        <h6>{{$profil->nama_pu}}<h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <h6>Addres<h6>
                                    </div>
                                    <div class="col-1">
                                        <h6>:<h6>
                                    </div>
                                    <div class="col-8">
                                        <h6>{{$profil->alamat_pu}}<h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <h6>Registration Number<h6>
                                    </div>
                                    <div class="col-1">
                                        <h6>:<h6>
                                    </div>
                                    <div class="col-8">
                                        <h6>{{$profil->id_reg}}<h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <h6>Jenis Usaha<h6>
                                    </div>
                                    <div class="col-1">
                                        <h6>:<h6>
                                    </div>
                                    <div class="col-8">
                                        <h6>{{$profil->jenis_usaha}}<h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <h6>Status<h6>
                                    </div>
                                    <div class="col-1">
                                        <h6>:<h6>
                                    </div>
                                    <div class="col-8">
                                        <h6>{{$profil->status_reg}}<h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <h6>Aksi<h6>
                                    </div>
                                    <div class="col-1">
                                        <h6>:<h6>
                                    </div>
                                    <div class="col-8">
                                        <form action="{{ route('updateApplication') }}" method="POST">
                                            @csrf
                                            <input 
                                            type="text" 
                                            class="form-control 
                                            form-control-xl" 
                                            name="id_reg" 
                                            value={{$profil->id_reg}}
                                            required
                                            hidden
                                            >
                                            <button class="btn btn-block btn-lg shadow-lg mt-5 text-light" style="background-color:#0F5132">Update Pengajuan</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div classs="row">
                    <div class="card">
                        <section class="section">
                            <div class="card">
                                <div class="card-header">
                                    <h4>List Produk</h4>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table" id="table1">
                                            <thead>
                                                <tr>
                                                    <th>Registration Id</th>
                                                    <th>Nama Produk</th>
                                                    <th>Publish</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($products AS $row)
                                                    <tr>
                                                        <td class="col-auto">
                                                            <p class="mb-0">{{$row->id_reg}}</p>
                                                        </td>
                                                        <td class="col-auto">
                                                            <p class=" mb-0">{{$row->reg_prod_name}}</p>
                                                        </td>
                                                        <td class="col-auto">
                                                            <p class=" mb-0">{{$row->reg_publish}}</p>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
