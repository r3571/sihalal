@extends('layouts.main')

@section('content')
<div class="page-heading">
    <h3>Application For LPH</h3>
</div>
<div class="page-content">
    <section class="row">
        <div class="col-12">
            {{-- <div class="row">
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon purple">
                                        <i class="iconly-boldShow"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Profile Views</h6>
                                    <h6 class="font-extrabold mb-0">112.000</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon blue">
                                        <i class="iconly-boldProfile"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Followers</h6>
                                    <h6 class="font-extrabold mb-0">183.000</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon green">
                                        <i class="iconly-boldAdd-User"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Following</h6>
                                    <h6 class="font-extrabold mb-0">80.000</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon red">
                                        <i class="iconly-boldBookmark"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Saved Post</h6>
                                    <h6 class="font-extrabold mb-0">112</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Description</h4>
                        </div>
                        <div class="card-body">
                            <table class="table" id="table1">
                                <thead>
                                    <tr>
                                        <th>Factory</th>
                                        <th>Registred Number</th>
                                        <th>Types of Products</th>
                                        <th>Service Type</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Indra Jaya Bakery</td>
                                        <td>SH2021-1-000028</td>
                                        <td>Kue kering</td>
                                        <td>Makanan</td>
                                        <td>
                                            <span class="badge bg-warning">Proses di LPH</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('appDtl') }}" class="btn btn-sm btn-info">
                                                <svg class="bi" width="1em" height="1em" fill="currentColor">
                                                    <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ira Bakery</td>
                                        <td>SH2021-1-000029</td>
                                        <td>Kue Basah</td>
                                        <td>Makanan</td>
                                        <td>
                                            <span class="badge bg-success">Selesai</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('appDtl') }}" class="btn btn-sm btn-info">
                                                <svg class="bi" width="1em" height="1em" fill="currentColor">
                                                    <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tom Bakery</td>
                                        <td>SH2021-1-000030</td>
                                        <td>Kue</td>
                                        <td>Makanan</td>
                                        <td>
                                            <span class="badge bg-success">Selesai</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('appDtl') }}" class="btn btn-sm btn-info">
                                                <svg class="bi" width="1em" height="1em" fill="currentColor">
                                                    <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kopi Cerita Abadi</td>
                                        <td>SH2021-1-000031</td>
                                        <td>Minuman Kopi</td>
                                        <td>Minuman</td>
                                        <td>
                                            <span class="badge bg-warning">Proses di LPH</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('appDtl') }}" class="btn btn-sm btn-info">
                                                <svg class="bi" width="1em" height="1em" fill="currentColor">
                                                    <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>PT kosmetik kita bersama</td>
                                        <td>SH2021-1-000031</td>
                                        <td>Kosmetik</td>
                                        <td>Kosmetik</td>
                                        <td>
                                            <span class="badge bg-warning">Proses di LPH</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('appDtl') }}" class="btn btn-sm btn-info">
                                                <svg class="bi" width="1em" height="1em" fill="currentColor">
                                                    <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>PT cantik gemilang</td>
                                        <td>SH2021-1-000034</td>
                                        <td>Kosmetik</td>
                                        <td>Kosmetik</td>
                                        <td>
                                            <span class="badge bg-warning">Proses di LPH</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('appDtl') }}" class="btn btn-sm btn-info">
                                                <svg class="bi" width="1em" height="1em" fill="currentColor">
                                                    <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Enak Catering</td>
                                        <td>SH2021-1-00035</td>
                                        <td>Lauk Pauk</td>
                                        <td>Makanan</td>
                                        <td>
                                            <span class="badge bg-success">Selesai</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('appDtl') }}" class="btn btn-sm btn-info">
                                                <svg class="bi" width="1em" height="1em" fill="currentColor">
                                                    <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Catring Kenyang</td>
                                        <td>SH2021-1-000036</td>
                                        <td>Makanan</td>
                                        <td>Makanan</td>
                                        <td>
                                            <span class="badge bg-success">Selesai</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('appDtl') }}" class="btn btn-sm btn-info">
                                                <svg class="bi" width="1em" height="1em" fill="currentColor">
                                                    <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>PT sehat bersama</td>
                                        <td>SH2021-1-000037</td>
                                        <td>Obat</td>
                                        <td>Obat</td>
                                        <td>
                                            <span class="badge bg-success">Selesai</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('appDtl') }}" class="btn btn-sm btn-info">
                                                <svg class="bi" width="1em" height="1em" fill="currentColor">
                                                    <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Apotek Jaya</td>
                                        <td>SH2021-1-000038</td>
                                        <td>Obat</td>
                                        <td>Obat</td>
                                        <td>
                                            <span class="badge bg-success">Selesai</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('appDtl') }}" class="btn btn-sm btn-info">
                                                <svg class="bi" width="1em" height="1em" fill="currentColor">
                                                    <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Boba enak jaya</td>
                                        <td>SH2021-1-000039</td>
                                        <td>Minuman Boba</td>
                                        <td>Minumam</td>
                                        <td>
                                            <span class="badge bg-success">Selesai</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('appDtl') }}" class="btn btn-sm btn-info">
                                                <svg class="bi" width="1em" height="1em" fill="currentColor">
                                                    <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ayam Bakar Pak Eko</td>
                                        <td>SH2021-1-000040</td>
                                        <td>Ayam Bakar</td>
                                        <td>Makanan</td>
                                        <td>
                                            <span class="badge bg-success">Selesai</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('appDtl') }}" class="btn btn-sm btn-info">
                                                <svg class="bi" width="1em" height="1em" fill="currentColor">
                                                    <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ayam Goreng wijaya</td>
                                        <td>SH2021-1-000041</td>
                                        <td>Ayam Goreng</td>
                                        <td>Makanan</td>
                                        <td>
                                            <span class="badge bg-success">Selesai</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('appDtl') }}" class="btn btn-sm btn-info">
                                                <svg class="bi" width="1em" height="1em" fill="currentColor">
                                                    <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Minum sehat</td>
                                        <td>SH2021-1-000042</td>
                                        <td>Minuman herbal</td>
                                        <td>Minuman</td>
                                        <td>
                                            <span class="badge bg-success">Selesai</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('appDtl') }}" class="btn btn-sm btn-info">
                                                <svg class="bi" width="1em" height="1em" fill="currentColor">
                                                    <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Apotek Kebon Nanas</td>
                                        <td>SH2021-1-000043</td>
                                        <td>Obat-obatan</td>
                                        <td>Obat</td>
                                        <td>
                                            <span class="badge bg-success">Selesai</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('appDtl') }}" class="btn btn-sm btn-info">
                                                <svg class="bi" width="1em" height="1em" fill="currentColor">
                                                    <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#book-fill"></use>
                                                </svg>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                {{-- <div class="col-12 col-xl-4">
                    <div class="card">
                        <div class="card-header">
                            <h4>Profile Visit</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="d-flex align-items-center">
                                        <svg class="bi text-primary" width="32" height="32" fill="blue" style="width:10px">
                                            <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#circle-fill" />
                                        </svg>
                                        <h5 class="mb-0 ms-3">Europe</h5>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <h5 class="mb-0">862</h5>
                                </div>
                                <div class="col-12">
                                    <div id="chart-europe"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="d-flex align-items-center">
                                        <svg class="bi text-success" width="32" height="32" fill="blue" style="width:10px">
                                            <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#circle-fill" />
                                        </svg>
                                        <h5 class="mb-0 ms-3">America</h5>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <h5 class="mb-0">375</h5>
                                </div>
                                <div class="col-12">
                                    <div id="chart-america"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="d-flex align-items-center">
                                        <svg class="bi text-danger" width="32" height="32" fill="blue" style="width:10px">
                                            <use xlink:href="assets/vendors/bootstrap-icons/bootstrap-icons.svg#circle-fill" />
                                        </svg>
                                        <h5 class="mb-0 ms-3">Indonesia</h5>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <h5 class="mb-0">1025</h5>
                                </div>
                                <div class="col-12">
                                    <div id="chart-indonesia"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
        {{-- <div class="col-12 col-lg-3">
            <div class="card">
                <div class="card-body py-4 px-5">
                    <div class="d-flex align-items-center">
                        <div class="avatar avatar-xl">
                            <img src="assets/images/faces/1.jpg" alt="Face 1">
                        </div>
                        <div class="ms-3 name">
                            <h5 class="font-bold">John Duck</h5>
                            <h6 class="text-muted mb-0">@johnducky</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4>Recent Messages</h4>
                </div>
                <div class="card-content pb-4">
                    <div class="recent-message d-flex px-4 py-3">
                        <div class="avatar avatar-lg">
                            <img src="assets/images/faces/4.jpg">
                        </div>
                        <div class="name ms-4">
                            <h5 class="mb-1">Hank Schrader</h5>
                            <h6 class="text-muted mb-0">@johnducky</h6>
                        </div>
                    </div>
                    <div class="recent-message d-flex px-4 py-3">
                        <div class="avatar avatar-lg">
                            <img src="assets/images/faces/5.jpg">
                        </div>
                        <div class="name ms-4">
                            <h5 class="mb-1">Dean Winchester</h5>
                            <h6 class="text-muted mb-0">@imdean</h6>
                        </div>
                    </div>
                    <div class="recent-message d-flex px-4 py-3">
                        <div class="avatar avatar-lg">
                            <img src="assets/images/faces/1.jpg">
                        </div>
                        <div class="name ms-4">
                            <h5 class="mb-1">John Dodol</h5>
                            <h6 class="text-muted mb-0">@dodoljohn</h6>
                        </div>
                    </div>
                    <div class="px-4">
                        <button class='btn btn-block btn-xl btn-light-primary font-bold mt-3'>Start
                            Conversation</button>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4>Visitors Profile</h4>
                </div>
                <div class="card-body">
                    <div id="chart-visitors-profile"></div>
                </div>
            </div>
        </div> --}}
    </section>
</div>
@endsection

