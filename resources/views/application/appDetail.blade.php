@extends('layouts.main')

@section('content')
<div class="page-heading">
    <h3>Application For LPH</h3>
</div>
<div class="page-content">
    <section class="row">
        <div class="col-12">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col">
                                    <h4>Detail</h4>
                                </div>
                                <div class="col d-flex justify-content-end">
                                    <INPUT class="font-bold text-white btn btn-primary rounded-lg p-2" TYPE="button" VALUE="Back" onClick="history.go(-1);" style="width: auto;">
                                </div>
                            </div>
                            {{-- <button type="button" class="btn btn-primary">
                            <svg class="svg-inline--fa fa-plus-square fa-w-14 fa-fw select-all" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="">
                                <path fill="currentColor" d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-32 252c0 6.6-5.4 12-12 12h-92v92c0 6.6-5.4 12-12 12h-56c-6.6 0-12-5.4-12-12v-92H92c-6.6 0-12-5.4-12-12v-56c0-6.6 5.4-12 12-12h92v-92c0-6.6 5.4-12 12-12h56c6.6 0 12 5.4 12 12v92h92c6.6 0 12 5.4 12 12v56z"></path>
                            </svg> Add
                            </button> --}}
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-lg">
                                    <thead>
                                        <tr>
                                            <th>Registration Id</th>
                                            <th>Description</th>
                                            <th>QTY</th>
                                            <th>Price</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="col-auto">
                                                <p class="mb-0">25682</p>
                                            </td>
                                            <td class="col-auto">
                                                <p class=" mb-0">Biaya Pemeriksaan Halal (Skala Indusri
                                                    Mikro, 34 Produk) + PPN 10%</p>
                                            </td>
                                            <td class="col-auto">
                                                <p class=" mb-0">1</p>
                                            </td>
                                            <td class="col-auto">
                                                <p class=" mb-0">5225000</p>
                                            </td>
                                            <td class="col-auto">
                                                <p class=" mb-0">5225000</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

