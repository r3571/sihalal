@extends('layouts.main')

@section('content')
<section class="section">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">List Yang Selesai Sidang Fatwa</h4>
        </div>
        <!-- Basic Tables start -->
		    <section class="section">
		        <div class="card">
		            <div class="card-body">
		                <table class="table" id="table1">
		                    <thead>
		                        <tr>
		                            <th>ID Reg</th>
		                            <th>Nama Pemohon</th>
		                            <th>Status Reg</th>
		                            <th>No Daftar</th>
		                            <th>Tgl Daftar</th>
		                            <th>Jenis Daftar</th>
		                            <th>Jenis Product</th>
		                            <th>Jumlah Produk</th>
		                            <th>Jenis Usaha</th>
		                            <th>No Urut NDPU</th>
		                        </tr>
		                    </thead>
		                    <tbody>
							@foreach($data->payload AS $row)
		                        <tr>
		                            <td>{{$row->id_reg}}</td>
		                            <td>{{$row->nama_pu}}</td>
		                            <td>{{$row->nama_status_reg}}</td>
		                            <td>{{$row->no_daftar}}</td>
		                            <td>{{$row->tgl_daftar}}</td>
		                            <td>{{$row->nama_jenis_daftar}}</td>
		                            <td>{{$row->nama_jenis_produk}}</td>
		                            <td>{{$row->jml_produk}}</td>
		                            <td>{{$row->nama_jenis_usaha}}</td>
									<td>{{$row->no_ndpu}}</td>
		                        </tr>
							@endforeach
		                    </tbody>
		                </table>
		            </div>
		        </div>
		    </section>
    </div>
</section>
@endsection