@extends('layouts.main')

<!-- @section('page-style')
  <link rel="stylesheet" href="/assets/css/process.css">
  @endsection -->
@section('content')
<div class="page-content">
    <section class="row">
        <div class="col-12 col-lg-9">
            <div class="row">
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon purple">
                                        <i class="iconly-boldShow"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Aplikasi</h6>
                                    <h6 class="font-extrabold mb-0">1</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon blue">
                                        <i class="iconly-boldFilter"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Produk</h6>
                                    <h6 class="font-extrabold mb-0">5</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon green">
                                        <i class="iconly-boldBookmark"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Aktif</h6>
                                    <h6 class="font-extrabold mb-0">1</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon red">
                                        <i class="iconly-boldBookmark"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Non Aktif</h6>
                                    <h6 class="font-extrabold mb-0">0</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Progres Status</h4>
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <ul class="timeline">
                                    <div>
                                        <li>
                                            <span class="year">Aplikasi</span>
                                            <div> 
                                                <ul class="content">
                                                    <li>
                                                    <vs-chip transparent color="success">progres</vs-chip><br>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="year">BPJPH Set Biaya</span>
                                            <div> 
                                                <ul class="content">
                                                    <li>
                                                    <vs-chip transparent color="success">progres</vs-chip><br>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="year">Penjadwalan</span>
                                            <div> 
                                                <ul class="content">
                                                    <li>
                                                    <vs-chip transparent color="success">progres</vs-chip><br>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="year">Audit</span>
                                            <div> 
                                                <ul class="content">
                                                    <li>
                                                    <vs-chip transparent color="success">progres</vs-chip><br>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="year">Keputusan MUI</span>
                                            <div> 
                                                <ul class="content">
                                                    <li>
                                                    <vs-chip transparent color="success">produk</vs-chip><br>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </div>
                                    <li><span class="year new">Finish</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-3">
            <div class="card">
                <div class="card-body py-4 px-5">
                    <div class="d-flex align-items-center">
                        <div class="avatar avatar-xl">
                            <img src="assets/images/faces/1.jpg" alt="Face 1">
                        </div>
                        <div class="ms-3 name">
                            <h5 class="font-bold">{{ Session::get('name') }}</h5>
                            <h6 class="text-muted mb-0">{{ Session::get('userid') }}</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4>Invoice</h4>
                </div>
                <div class="card-content pb-4">
                    <div class="recent-message d-flex px-4 py-3">
                        <div class="avatar avatar-lg">
                          <i class="iconly-boldBookmark"></i>
                        </div>
                        <div class="name ms-4">
                            <h5 class="mb-1">No. Invoice : 21000002</h5>
                            <h6 class="text-muted mb-0">Batas : 2021-04-03</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
