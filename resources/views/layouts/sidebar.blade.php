<div id="app">
    <div id="sidebar" class="active">
        <div class="sidebar-wrapper active">
            <div class="sidebar-header">
                <div class="d-flex justify-content-between">
                    <div class="logo">
                        <a href="{{ route('index') }}"><img src="{{asset('assets/images/logo/mujahidin.png') }}" alt="Logo" style="width: 80%; height: auto;"></a>
                    </div>
                    <div class="toggler">
                        <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                    </div>
                </div>
            </div>
            <div class="sidebar-menu"> 
                <ul class="menu">
                    <li class="sidebar-item {{ Request::is('dashboard') ? 'active' : '' }}">
                        <a href="{{ route('index') }}" class='sidebar-link'>
                            <i class="bi bi-grid-fill"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>

                    <li class="sidebar-item {{ Request::is('profil*') ? 'active' : '' }}">
                        <a href="{{ route('profile') }}" class='sidebar-link'>
                            <i class="bi bi-person-fill"></i>
                            <span>Profil</span>
                        </a>
                    </li>
                    <li class="sidebar-title">Aktivitas</li>

                    <li class="sidebar-item {{ Request::is('aplikasi*') ? 'active' : '' }}">
                        <a href="{{ route('applications') }}" class='sidebar-link'>
                            <i class="bi bi-stack"></i>
                            <span>Data Permohonan</span>
                        </a>
                    </li>
                    <li class="sidebar-item {{ Request::is('biaya*') ? 'active' : '' }}">
                        <a href="{{ route('costs') }}" class='sidebar-link'>
                            <i class="bi bi-cash"></i>
                            <span>Biaya</span>
                        </a>
                    </li>
                    <li class="sidebar-item {{ Request::is('audit*') ? 'active' : '' }}">
                        <a href="{{ route('audit') }}" class='sidebar-link'>
                            <i class="bi bi-pen-fill"></i>
                            <span>Audit</span>
                        </a>
                    </li>
                    <li class="sidebar-item {{ Request::is('lph*') ? 'active' : '' }}">
                        <a href="{{ route('lph') }}" class='sidebar-link'>
                            <i class="bi bi-grid-1x2-fill"></i>
                            <span>Selesai Proses LPH</span>
                        </a>
                    </li>
                    <li class="sidebar-item {{ Request::is('fatwa*') ? 'active' : '' }}">
                        <a href="{{ route('fatwa') }}" class='sidebar-link'>
                            <i class="bi bi-grid-1x2-fill"></i>
                            <span>Selesai Sidang Fatwa</span>
                        </a>
                    </li>
                </ul>
            </div>
