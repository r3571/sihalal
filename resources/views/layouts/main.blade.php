<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Apps - Halal</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/bootstrap.css">
    <link rel="stylesheet" href="/assets/vendors/iconly/bold.css">
    <link rel="stylesheet" href="/assets/vendors/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="/assets/vendors/bootstrap-icons/bootstrap-icons.css">
    <link rel="stylesheet" href="/assets/css/app.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="shortcut icon" href="/assets/images/favicon.svg" type="image/x-icon">
    <link rel="stylesheet" href="/assets/vendors/jquery-datatables/jquery.dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="/assets/vendors/fontawesome/all.min.css">
    <link rel="stylesheet" href="/assets/css/process.css">
</head>

<body>
    @include('layouts.sidebar')
    <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
</div>
        </div>
        <div id="main">
            <header class="mb-3 navbar fixed-top navbar-light sticky-top bg-white flex-md-nowrap">
                <a href="#" class="burger-btn d-block d-xl-none">
                    <i class="bi bi-justify fs-3"></i>
                </a>
                @include('layouts.navbar')
            </header>
            <div class="mb-5"></div>
            @yield('content')
            @include('layouts.footer')
        </div>
    </div>
    <script src="/assets/vendors/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="/assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="/assets/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/vendors/apexcharts/apexcharts.js"></script>
    <script src="/assets/js/pages/dashboard.js"></script>
    <script src="/assets/js/mazer.js"></script>
    <script src="/assets/vendors/jquery-datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/vendors/jquery-datatables/custom.jquery.dataTables.bootstrap5.min.js"></script>
    <script src="/assets/vendors/fontawesome/all.min.js"></script>
</body>
    <script>
        // Jquery Datatable
        let jquery_datatable = $("#table1").DataTable();
        let jquery_datatable2 = $("#table2").DataTable();
        $(".dateRangePicker").daterangepicker({
            "singleDatePicker": true,
            "autoApply": true,
            "locale": {
                "format": "MM/DD/YYYY"
            }
        });

        // var a = moment('2007-10-29');
        // var b = moment('2007-10-27');
        // console.log(a.diff(b, 'days')+1);   // =1
        
        $('#jadwal_akhir').change(function(){
            // let jadwalAwal = new Date($('#jadwal_awal').val());
            // let jadwalAkhir = new Date($('#jadwal_akhir').val());

            // const days = (jadwalAwal, jadwalAkhir) =>{
            //     let difference = jadwalAkhir.getTime() - jadwalAwal.getTime();
            //     let TotalDays = Math.ceil(difference / (1000 * 3600 * 24));
            //     return TotalDays;
            // }
            // $('#jml_hari').val(days(jadwalAwal, jadwalAkhir));
        });
    </script>

</html>