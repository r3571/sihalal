@extends('layouts.main')

@section('content')
<div class="page-content">
    <section class="row">
        <div class="col-12">              
            <div class="card">
                <div class="card-body">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Biaya & Invoice</h5>
                        </div>
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" 
                                        id="biaya-tab" 
                                        data-bs-toggle="tab" 
                                        href="#biaya" 
                                        role="tab" 
                                        aria-controls="biaya" 
                                        aria-selected="true">Biaya
                                    </a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" 
                                        id="produk-tab" 
                                        data-bs-toggle="tab" 
                                        href="#produk" 
                                        role="tab" 
                                        aria-controls="produk" 
                                        aria-selected="false">Invoice
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div 
                                    class="tab-pane fade show active" 
                                    id="biaya" 
                                    role="tabpanel" 
                                    aria-labelledby="biaya-tab"
                                >
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <div class="mb-3">
                                        <button class="btn btn-outline-success block" onclick="history.back()"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left-square-fill" viewBox="0 0 16 16">
                                        <path d="M16 14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12zm-4.5-6.5H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5a.5.5 0 0 0 0-1z"/>
                                        </svg> Kembali</button>
                                            <button type="button" class="btn btn-outline-success block" data-bs-toggle="modal"
                                                data-bs-target="#exampleModalCenter">Tambah <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-square-fill" viewBox="0 0 16 16">
                                                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3a.5.5 0 0 1 1 0z"/>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                                            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
                                                role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Biaya
                                                        </h5>
                                                        <button type="button" class="close" data-bs-dismiss="modal"
                                                            aria-label="Close">
                                                            <i data-feather="x"></i>
                                                        </button>
                                                    </div>
                                                    <form action="{{ route('createCost') }}" method="POST">
                                                    @csrf
                                                    <div class="modal-body">
                                                    <div class="form-group">
                                                            <input 
                                                                type="text"
                                                                class="form-control" 
                                                                name="id_reg" 
                                                                id="basicInput" 
                                                                value={{$id}}
                                                                hidden>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="basicInput">Keterangan</label>
                                                            <input 
                                                                type="text"
                                                                class="form-control" 
                                                                name="keterangan" 
                                                                id="basicInput" 
                                                                placeholder="Biaya kegiatan">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="helpInputTop">Kuantitas</label>
                                                            <input 
                                                                type="text" 
                                                                class="form-control" 
                                                                name="qty" 
                                                                id="helpInputTop" 
                                                                placeholder="2">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="helperText">Harga</label>
                                                            <input 
                                                                type="text" 
                                                                id="helperText" 
                                                                name="harga"
                                                                class="form-control" 
                                                                placeholder="1000">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-light-secondary"
                                                            data-bs-dismiss="modal">
                                                            <i class="bx bx-x d-block d-sm-none"></i>
                                                            <span class="d-none d-sm-block">Keluar</span>
                                                        </button>
                                                        <button 
                                                            type="submit" 
                                                            class="btn btn-primary ml-1">
                                                            <i class="bx bx-check d-block d-sm-none"></i>
                                                            <span class="d-none d-sm-block">Simpan</span>
                                                        </button>
                                                    </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table" id="table1">
                                            <thead>
                                                <tr>
                                                    <th>Keterangan</th>
                                                    <th>kuantitas</th>
                                                    <th>Harga</th>
                                                    <th>Total</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($costs->payload AS $row)
                                                @if($row->id_reg == $id)
                                                <tr>
                                                    <td>{{$row->keterangan}}</td>
                                                    <td>{{$row->qty}}</td>
                                                    <td>{{$row->harga}}</td>
                                                    <td>{{$row->total}}</td>
                                                    <td><a href="{{ route('deleteCost', array($row->id_biaya, $id)) }}" class="btn icon btn-danger">Hapus</a></td>
                                                </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                </div>
                                <div class="tab-pane fade" id="produk" role="tabpanel" aria-labelledby="produk-tab">
                                    <div class="mb-3 mt-3">
                                        <button class="btn btn-outline-success block" onclick="history.back()"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left-square-fill" viewBox="0 0 16 16">
                                        <path d="M16 14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12zm-4.5-6.5H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5a.5.5 0 0 0 0-1z"/>
                                        </svg> Kembali</button>
                                            <button type="button" class="btn btn-outline-success block" data-bs-toggle="modal"
                                                data-bs-target="#exampleModalCenter">Tambah <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-square-fill" viewBox="0 0 16 16">
                                                <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3a.5.5 0 0 1 1 0z"/>
                                                </svg>
                                            </button>
                                        </div>
                                    <div class="table-responsive">
                                        <table class="table" id="table2">
                                            <thead>
                                                <tr>
                                                    <th>No. Invoice</th>
                                                    <th>Tanggal</th>
                                                    <th>Tipe</th>
                                                    <th>Alamat</th>
                                                    <th>Telepon</th>
                                                    <th>Batas Inv</th>
                                                    <th>Status</th>
                                                    <th>Total</th>
                                                    <th>File</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($inv->payload AS $row)
                                                <tr>
                                                    <td>{{$row->no_inv}}</td>
                                                    <td>{{$row->tgl_inv}}</td>
                                                    <td>{{$row->tipe_trans}}</td>
                                                    <td>{{$row->alamat1}}</td>
                                                    <td>{{$row->No_telp}}</td>
                                                    <td>{{$row->duedate}}</td>
                                                    <td>{{$row->status_payment}}</td>
                                                    <td>{{$row->total_inv}}</td>
                                                    <td>{{$row->file_inv}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
