<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\AplikasiController;
use App\Http\Controllers\BiayaController;
use App\Http\Controllers\AuditController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthController::class, 'index'])->name('login')->middleware('guest');

Route::post('/authenticate', [AuthController::class, 'authenticate'])->name('authenticate');

Route::get('/forgetpassword', [AuthController::class, 'forget'])->name('forget');

Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

Route::get('/dashboard', [DashboardController::class, 'index'])->name('index');

Route::get('/profil', [ProfilController::class, 'index'])->name('profile');

Route::get('/aplikasi', [AplikasiController::class, 'index'])->name('applications');

Route::get('/aplikasi/detail/{id}', [AplikasiController::class, 'detail'])->name('detail');

Route::post('/update', [AplikasiController::class, 'update'])->name('updateApplication');

Route::get('/biaya', [BiayaController::class, 'index'])->name('costs');

Route::get('/biaya/detail/{id}', [BiayaController::class, 'detail'])->name('cost');

Route::post('/create', [BiayaController::class, 'create'])->name('createCost');

Route::get('/delete/{id}/{reg}', [BiayaController::class, 'delete'])->name('deleteCost');

Route::get('/audit', [AuditController::class, 'index'])->name('audit');

Route::get('/audit/detail/{id}', [AuditController::class, 'detail'])->name('detailAudit');

Route::post('/audit/create', [AuditController::class, 'create'])->name('create');

Route::get('/audit/delete/{idAudit}', [AuditController::class, 'deleteAudit'])->name('deleteAudit');

Route::post('/audit/auditor', [AuditController::class, 'createAuditor'])->name('AddAuditor');

Route::get('/lph', [AplikasiController::class, 'lph'])->name('lph');

Route::get('/fatwa', [AplikasiController::class, 'fatwa'])->name('fatwa');