<?php

namespace App\Helpers;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Session;
// use Session;

class Curl
{
    public function __construct() {
        $this->uri = env("APP_API");
        $this->token = Session::get('token');
        $this->refreshToken = Session::get('refreshToken');
    }

    public function link($path)
    {
		return $this->uri.$path;
	}

    public function simple_get($path)
    {
        $body = [
            'headers' => [
                'Authorization' => '__bpjph_ct='.$this->token.'; __bpjph_rt='.$this->refreshToken
            ]
        ];
      
        return $this->execute('GET', $this->link($path), $body);
    }

    public function simple_put($path, $params)
    {
        $body = [
            'json' => $params,
            'headers' => [
                'Authorization' => '__bpjph_ct='.$this->token.'; __bpjph_rt='.$this->refreshToken
            ]
        ];

        return $this->execute('PUT', $this->link($path), $body);
    }

    public function simple_delete($path)
    {
        $body = [
            'headers' => [
                'Authorization' => '__bpjph_ct='.$this->token.'; __bpjph_rt='.$this->refreshToken
            ]
        ];

        return $this->execute('DELETE', $this->link($path), $body);
    }

    public function simple_post($path, $params)
    {
        $body = [
            'json' => $params,
            'headers' => [
                'Authorization' => '__bpjph_ct='.$this->token.'; __bpjph_rt='.$this->refreshToken
            ]
        ];

        return $this->execute('POST', $this->link($path), $body);
    }

    public function execute($method, $uri, $body)
    {
        $client = new Client();
        $res = $client->request($method, $uri, $body);

        $respons =  $res->getBody();
        return json_decode($respons);
    }
}


