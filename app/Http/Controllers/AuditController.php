<?php

namespace App\Http\Controllers;
use App\Helpers\Curl;

use Illuminate\Http\Request;

class AuditController extends Controller
{
    
    public function index()
    {
        $curl = new Curl();
        $return = $curl->simple_get('/api/v1/applications/10040/'.env("ID_LPH"));

        return view('audit.index', ['data' => $return->payload]);
    }

    public function detail($idReg)
    {
        $curl = new Curl();
        $return = $curl->simple_get('/api/v1/audit');
        $detail = $curl->simple_get('/api/v1/application/detail/'.$idReg);
        $detailAuditor = $curl->simple_get('/api/v1/auditors/'.$detail->payload->lph_id);
        //list auditor
        $auditor = $curl->simple_get('/api/v1/auditor');
       
        $data['audit'] = $return->payload;
        $data['auditor'] = $detailAuditor->payload;
        $data['idReg'] = $idReg;
        return view('audit.detail',$data);
    }

    public function create(request $request)
    {
        $body = [
            'id_reg' => $request->id_reg,
            'jadwal_awal' => $request->jadwal_awal,
            'jadwal_akhir' => $request->jadwal_akhir,
            'jml_hari' => $request->jml_hari
        ];

        $curl = new Curl();
        $return = $curl->simple_post('/api/v1/audit', $body);
        // dd($return);

        if($return->status){
            return redirect('/audit/detail/'.$request->id_reg)->with('success', 'Jadwal Audit Berhasil di Tambahkan');
        }

        return redirect('/audit/detail/'.$request->id_reg)->with('error', 'Jadwal Audit Gagal di Tambahkan');
    }

    public function createAuditor(request $request)
    {
        $body = [
            'id_reg' => $request->id_reg,
            'auditor_id' => $request->auditor_id,
            'created_by' => $request->lph,
        ];
        
        $curl = new Curl();
        $return = $curl->simple_post('/api/v1/auditor', $body);
        // dd($return);

        if($return->status){
            return redirect('/audit/detail/'.$request->id_reg)->with('success', 'Auditor Berhasil di Tambahkan');
        }

        return redirect('/audit/detail/'.$request->id_reg)->with('error', 'Auditor Gagal di Tambahkan');
    }

    public function deleteAudit($id_audit)
    {
        $curl = new Curl();
        $return = $curl->simple_delete('/api/v1/audit/'.$id_audit);
 
        if($return->status){
            return redirect('/audit/detail/$id_reg')->with('success', 'Jadwal Berhasil di Hapus');
        }

        return redirect('/audit/detail/$id_reg')->with('error', 'Jadwal Gagal di Hapus');
    }


}
