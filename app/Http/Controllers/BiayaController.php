<?php

namespace App\Http\Controllers;
use App\Helpers\Curl;

use Illuminate\Http\Request;

class BiayaController extends Controller
{
    
    public function index()
    {
        $curl = new Curl();
        $return = $curl->simple_get('/api/v1/applications/10020/'.env("ID_LPH"));

        return view('cost.cost', ['data' => $return->payload]);
    }

    public function detail($id)
    {
        $curl = new Curl();
        $return = $curl->simple_get('/api/v1/costs');
        $returnInv = $curl->simple_get('/api/v1/invoices/'.env("ID_LPH"));

        $data['costs'] = $return->payload;
        $data['inv'] = $returnInv->payload;
        $data['id'] = $id;
        return view('cost.detail', $data);
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {      
        $body = [
            'id_reg' => $request->id_reg,
            'keterangan' => $request->keterangan,
            'qty' => $request->qty,
            'harga' => $request->harga
        ];

        $curl = new Curl();
        $return = $curl->simple_post('/api/v1/cost', $body);

        if($return->status){
            return redirect()->route('cost', $request->id_reg);
        }

        return redirect()->route('cost', $request->id_reg);
    }

    public function delete($id, $reg)
    {
        $curl = new Curl();
        $return = $curl->simple_delete('/api/v1/cost/'.$id);
 
        if($return->status){
            return redirect()->route('cost', $reg);
        }

        return redirect()->route('cost', $reg);
    }

}
