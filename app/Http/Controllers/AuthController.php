<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Helpers\Curl;
use Illuminate\Http\Request;
// use Session;

class AuthController extends Controller
{
    
    public function index()
    {
        return view('auth.login');
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {

        $request->validate([
            'email' => 'required|email:dns',
            'password' => 'required',
        ]);

        $body = [
            'email' => $request->email,
            'password' => $request->password
        ];

        $curl = new Curl();
        $return = $curl->simple_post('/api/v1/login', $body);
        // dd($curl);
        if($return->status) {
            Session::put('userid', $return->payload->userid);
            Session::put('name', $return->payload->nama);
            Session::put('token', $return->payload->token);
            Session::put('refreshToken', $return->payload->refreshToken);
            Session::put('role', $return->payload->role);
      
            return redirect()->route('index');
        };

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);      
    }

    public function forget(Request $request)
    {
    
        return redirect('auth.forget');
    }

    public function logout(Request $request)
    {
        
        Session::flush();
        return redirect('/');
    }
}
