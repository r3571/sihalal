<?php

namespace App\Http\Controllers;
use App\Helpers\Curl;
use Session;

use Illuminate\Http\Request;

class AplikasiController extends Controller
{
    
    public function index()
    {
        $curl = new Curl();
        $return = $curl->simple_get('/api/v1/applications/10010/'.env("ID_LPH"));

        return view('application.application', ['data' => $return->payload]);
    }

    public function detail($id)
    {
        $curl = new Curl();
        $return = $curl->simple_get('/api/v1/application/detail/'.$id);

        $data['profil'] = $return->payload;
        $data['products'] = $return->payload->products;

        return view('application.applicationDetail', $data);
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $body = [
            'reg_id' => $request->id_reg,
            'lph_mapped_id' => env("ID_LPH")
        ];

        $curl = new Curl();
        $return = $curl->simple_post('/api/v1/application', $body);
        // dd($return);
        if($return->payload->status){

            return redirect()->route('applications')->with('Success', 'Data Berhasil Diperbaharui');
        }

        return redirect()->route('detail')->with('Error', 'Data Gagal Diperbaharui');
    }

    public function lph()
    {
        $curl = new Curl();
        $return = $curl->simple_get('/api/v1/applications/10040/'.env("ID_LPH"));

        return view('application.applicationLPH', ['data' => $return->payload]);
    }

    public function fatwa()
    {
        $curl = new Curl();
        $return = $curl->simple_get('/api/v1/applications/10070/'.env("ID_LPH"));

        return view('application.applicationFatwa', ['data' => $return->payload]);
    }

}
